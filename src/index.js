import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import registerServiceWorker from './registerServiceWorker.js'
//import { constAndLet } from "./examples/constAndLet.js";
//import { arrowFunctions } from "./examples/arrowFunctions.js"
// import { destructuring } from "./examples/destructuring.js"
//import { objectAssemblyComputedProps } from "./examples/objAssemblyComputedProps.js"

//objectAssemblyComputedProps()


ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();
